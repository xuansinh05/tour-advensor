/** @format */

import React, { Component } from 'react'
import { createBrowserHistory } from 'history'
import { Router, Switch, Route } from 'react-router'
import routes from 'views/routes'
import { Header } from './views/HomePage/Header/Header' 
import { Nav } from 'views/HomePage/Header/Nav/Nav'
import { Footer } from 'views/HomePage/Footer/Footer'
import { Contact } from 'components/Contact/Contact'
const history = createBrowserHistory()

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <Nav />
        <div>
          <div>{this.showContentMenus(routes)}</div>
        </div>
        <Contact />
        <Footer />
      </Router>
    )
  }

  showContentMenus = (routes) => {
    var result = null
    if (routes.length > 0) {
      result = routes.map((route, index) => {
        return (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        )
      })
    }
    return <Switch>{result}</Switch>
  }
}

export default App
