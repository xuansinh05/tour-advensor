import React, { useState, useEffect, Fragment } from 'react'
import './DetailTour.scss'
import trip1 from 'assets/img/trip3.jpg'
import TimeLine from 'components/TimeLine'
import MapLeaf from 'components/Map'
import callApi from 'utils/apiCaller'
import { useParams } from 'react-router'

export default function DetaiTour() {
  const { id } = useParams()
  console.log('=========================sedfsdfsdf', id)
  const [tour, setTour] = useState([])
  useEffect(() => {
    callApi(`tour/GetAllTours/${id}`, 'GET', null).then((res) => {
      console.log(res.data)
      setTour(res.data)
      console.log(res.data)
    })
  }, [])

  return (
    <div className="container-tour">
      {tour.map((item, index) => (
        <Fragment key={index}>
          <h3>{item.name}</h3>
          <div className="tour-detail">
            <section className="slide-image">
              <div className="big-image">
                <img
                  style={{ width: '350px', height: '350px' }}
                  src={item.imageUrl}
                  alt=""
                />
              </div>
              <div className="small-imgae"></div>
            </section>
            <section className="content-tour">
              <div className="detail-tour">
                <div className="detail">Description</div>
                <div className="main-detail">
                  {item.description.slice(1, 500)}
                </div>
              </div>
              <div className="infor-tour">
                <div className="tour-info">Details</div>
                <div className="tour-price">Price: {item.price} </div>
                <div className="tour-location">
                  StartLocation: {item.startLocation}{' '}
                </div>
                <div className="tour-time">TimeSpend: {item.timeSpend} day</div>
                <div className="tour-rate"> Rating: {item.rate} star </div>
              </div>
            </section>
          </div>
          <h4>Lịch trình di chuyển</h4>
          <div className="time-line">
            <TimeLine
            />
            <div className="map">
              <MapLeaf locations={item.locations} />
            </div>
          </div>
        </Fragment>
      ))}
    </div>
  )
}
