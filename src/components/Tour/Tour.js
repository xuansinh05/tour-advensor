import React from 'react'
import classes from './Tour.module.scss'

export const Tour = (props) => {
  const { label, text, img } = props
  return (
    <div>
      <div style={{ backgroundImage: `url(${img})` }} className={classes.Card}>
        <div className={classes.CardText}>
          <h3>{label}</h3>
          <p>{text}</p>
        </div>
      </div>
    </div>
  )
}
