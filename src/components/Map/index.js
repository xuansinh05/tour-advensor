import React from 'react'
import L from 'leaflet'
import PropTypes from 'prop-types'
import Map from './Map'

const MapLeaf = (props) => {
  const { locations = [] } = props
  function createTripPointsGeoJson({ locations } = {}) {
    return {
      type: 'FeatureCollection',
      features: locations.map(
        ({ placename, location = {}, image, date, todo = [] } = {}) => {
          const { lat, lng } = location
          return {
            type: 'Feature',
            properties: {
              placename,
              todo,
              date,
              image,
            },
            geometry: {
              type: 'Point',
              coordinates: [lng, lat],
            },
          }
        }
      ),
    }
  }

  /**
   * tripStopPointToLayer
   */

  function createTripLinesGeoJson({ locations } = {}) {
    return {
      type: 'FeatureCollection',
      features: locations.map((stop = {}, index) => {
        const prevStop = locations[index - 1]

        if (!prevStop) return []

        const { placename, location = {}, date, todo = [] } = stop
        const { lat, lng } = location
        const properties = {
          placename,
          todo,
          date,
        }

        const { location: prevLocation = {} } = prevStop
        const { lat: prevLat, lng: prevLng } = prevLocation

        return {
          type: 'Feature',
          properties,
          geometry: {
            type: 'LineString',
            coordinates: [
              [prevLng, prevLat],
              [lng, lat],
            ],
          },
        }
      }),
    }
  }

  /**
   * tripStopPointToLayer
   */

  function tripStopPointToLayer(feature = {}, latlng) {
    const { properties = {} } = feature
    const { placename, todo = [], image, date } = properties

    const list = todo.map((what) => `<li>${what}</li>`)
    let listString = ''
    let imageString = ''

    if (Array.isArray(list) && list.length > 0) {
      listString = list.join('')
      listString = `
        <p>Things we will or have done...</p>
        <ul>${listString}</ul>
      `
    }

    if (image) {
      imageString = `
        <span class="trip-stop-image" style="background-image: url(${image})">${placename}</span>
      `
    }

    const text = `
      <div class="trip-stop">
        ${imageString}
        <div class="trip-stop-content">
          <h2>${placename}</h2>
          <p class="trip-stop-date">${date}</p>
          ${listString}
        </div>
      </div>
    `

    const popup = L.popup({
      maxWidth: 400,
    }).setContent(text)

    const layer = L.marker(latlng, {
      icon: L.divIcon({
        className: 'icon',
        html: `<span class="icon-trip-stop"></span>`,
        iconSize: 20,
      }),
      riseOnHover: true,
    }).bindPopup(popup)

    return layer
  }
  function mapEffect({ leafletElement: map } = {}) {
    if (!map) return

    map.eachLayer((layer) => map.removeLayer(layer))

    const tripPoints = createTripPointsGeoJson({ locations })
    const tripLines = createTripLinesGeoJson({ locations })

    const tripPointsGeoJsonLayers = new L.geoJson(tripPoints, {
      pointToLayer: tripStopPointToLayer,
    })

    const tripLinesGeoJsonLayers = new L.geoJson(tripLines)

    const bounds = tripPointsGeoJsonLayers.getBounds()

    tripPointsGeoJsonLayers.addTo(map)
    tripLinesGeoJsonLayers.addTo(map)

    map.fitBounds(bounds)
  }

  const mapSettings = {
    defaultBaseMap: 'OpenStreetMap',
    mapEffect,
  }

  return (
    <>
      <Map {...mapSettings} />
    </>
  )
}

export default MapLeaf

/**
 * tripStopPointToLayer
 */

MapLeaf.propTypes = {
  location: PropTypes.array,
}
