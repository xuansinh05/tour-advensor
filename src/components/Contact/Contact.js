import React, { useState } from 'react'
import classes from './Contact.module.scss'
import { Waypoint } from 'react-waypoint'
import { ContactItem } from '../ContactItem/ContactItem'
import { ContactInput } from '../ContactInputs/ContactInput/ContactInput'
import { ContactTextarea } from '../ContactInputs/ContactTextarea/ContactTextarea'

import Container from '../Container/Container'

export const Contact = () => {
  const [isEntered, setIsEntered] = useState(false)
  const handleWaypointEnter = () => {
    setIsEntered(true)
  }
  const handleWaypointLeave = () => {
    setIsEntered(false)
  }
  return (
    <section id="contact" className={classes.Background}>
      <Waypoint onEnter={handleWaypointEnter} onLeave={handleWaypointLeave} />
      <Container>
        <div className={classes.Contact}>
          <div
            className={`${classes.Contact_Details} ${
              isEntered ? classes.slideInLeft : ''
            }`}
          >
            <h4>Contact Info:</h4>
            <p className={classes.Contact_Details_Desciption}>
              Best hotel entrance ever! Almaha Marrakech is in the medina, down
              a side street with a barely-marked door. When you knock you’re let
              into this small room with a check-in desk and a wall of
              basket-lined shelves
            </p>
            <ContactItem label="Address">03 Quang Trung</ContactItem>
            <ContactItem label="Phone">(84) 327337125</ContactItem>
            <ContactItem label="Email">nguyenxuansinh0507@mail.com</ContactItem>
          </div>
          <form
            action=""
            className={`${classes.Contact_Form} ${
              isEntered ? classes.slideInRight : ''
            }`}
          >
            <div className={classes.Contact_Form_TwoCol}>
              <ContactInput placeholder="Your Name" />
              <ContactInput placeholder="Your Email" />
            </div>
            <ContactInput placeholder="Your Title" />
            <ContactTextarea placeholder="Your Comment" />
            <button>send massage</button>
          </form>
        </div>
      </Container>
    </section>
  )
}
