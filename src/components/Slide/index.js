import React from 'react'
import { Slide } from 'react-slideshow-image'
import slide1 from '../../assets/img/trip3.jpg'
import slide2 from '../../assets/img/trip5.jpg'
import slide3 from '../../assets/img/trip6.jpg'
import slide4 from '../../assets/img/trip7.jpg'
import './style.scss'

const properties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  arrows: false,
  autoplay: true,
  pauseOnHover: true,
}

const SildeShow = () => {
  return (
    <Slide {...properties}>
      <div className="each-slide">
        <div
          style={{
            backgroundImage: `url(${slide1})`,
          }}
        >
          <span>Da Nang Trip travel</span>
        </div>
      </div>
      <div className="each-slide">
        <div style={{ backgroundImage: `url(${slide2})` }}>
          <span>Ba Na Hill</span>
        </div>
      </div>
      <div className="each-slide">
        <div style={{ backgroundImage: `url(${slide3})` }}>
          <span>Quy Nhon</span>
        </div>
      </div>
      <div className="each-slide">
        <div style={{ backgroundImage: `url(${slide4})` }}>
          <span>Hoi An</span>
        </div>
      </div>
    </Slide>
  )
}

export default SildeShow
