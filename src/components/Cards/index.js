/** @format */

import React, { Fragment, useState, useEffect } from 'react'
import './style.scss'
import CardTour from './CardTour'
import callApi from 'utils/apiCaller'
import LinearProgress from '@material-ui/core/LinearProgress'
import { NavLink } from 'react-router-dom'

function Card(props) {
  const [tours, setTour] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [error, setError] = useState(false)
  useEffect(() => {
    callApi('tour/GetAllTours', 'GET', null)
      .then((res) => {
        console.log(res.data)
        setIsLoading(true)
        setTour(res.data)
        setIsLoading(false)
      })
      .catch((error) => {
        setError(true)
        console.log(error)
      })
  }, [])

  const { match } = props
  console.log('============MATCH==========', match)

  return (
    <>
      <h3>Tour được yêu thích nhất</h3>
      {isLoading && (
        <div className="icon-loading">
          <LinearProgress variant="query" color="secondary" />
        </div>
      )}
      {error && (
        <div className="error-loading">Something wrong when load API</div>
      )}
      <div className="card__item">
        {tours.map((item, index) => (
          <NavLink
            to={`${match.url}/${item.tourId}`}
            key={index}
            className="nav-link"
          >
            <Fragment>
              <CardTour
                tourId={item.tourId}
                name={item.name}
                imageUrl={item.imageUrl}
                description={item.description}
                price={item.price}
                startLocation={item.startLocation}
              />
            </Fragment>
          </NavLink>
        ))}
      </div>
    </>
  )
}

export default Card
