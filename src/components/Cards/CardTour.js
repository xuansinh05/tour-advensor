/** @format */

import React from 'react'
import './style.scss'
import { IoIosHeartEmpty, IoMdGlobe } from 'react-icons/io'
import { GoLocation } from 'react-icons/go'
import { AiOutlineDollar, AiFillCheckCircle } from 'react-icons/ai'
import { NavLink } from 'react-router-dom'

function CardTour(props) {
  const { tourId, name, imageUrl, description, price, startLocation } = props
  return (
    <div className="grid__item">
      <article className="card card-list">
        <div
          className="card__image"
          style={{ backgroundImage: `url(${imageUrl})` }}
        >
          <div className="icon-heart">
            <IoIosHeartEmpty color="white" size="30px" opacity="1" />
          </div>
        </div>
        <div className="card__content">
          <div className="card__title">
            {name}
            <AiFillCheckCircle color="#ff4d55" style={{ paddingLeft: '5px' }} />
          </div>
          <div className="card__description">{description}</div>
          <div className="card__price">Price: {price}</div>
          <div className="card__footer">
            <div className="address">
              <GoLocation color="#ff4d55" size="20px" />
              <div className="card__address">{startLocation}</div>
            </div>
            <div className="">
              <AiOutlineDollar color="#ff4d55" size="30px" />
              <IoMdGlobe color="#ff4d55" size="30px" />
            </div>
          </div>
        </div>
      </article>
    </div>
  )
}

export default CardTour
