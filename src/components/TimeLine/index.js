import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import StepContent from '@material-ui/core/StepContent'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '30%',
    fontFamily:
      '-apple-system, BlinkMacSystemFont,Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell,Open Sans,Helvetica Neue, sans-serif',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
  completed: {
    fontSize: '20px',
  },
  active: {
    fontSize: '30px',
  },
  colorTextPrimary: {
    fontSize: '20px',
  },
  body1: {
    fontSize: '20px',
    color: 'grey',
  },
  label: {
    fontSize: '20px',
  },
}))
function getSteps() {
  return ['Đà Nẵng ', 'Hội An', 'Quy Nhơn', 'Bình Định', 'Phú Yên']
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return `Đi chùa, đi chơi, cùng với những ngườ`
    case 1:
      return 'Gia Lai là tỉnh có diện tích lớn thứ 2 Việt Nam và là một tỉnh miền núi nằm ở khu vực phía bắc của vùng Tây Nguyên,'
    case 2:
      return `Cần Thơ là một thành phố trực thuộc Trung ương của Việt Nam, là thành phố hiện đại và phát triển nhất ở Đồng bằng sông Cửu Long`
    case 3:
      return `Đã có dịp ghé thăm chùa Thiên Mụ, mình rất ấn tượng với quang cảnh nơi đây.`
    case 4:
      return `Nagaworld là khách sạn đẹp nhất Campuchia.Phòng khách sạn rất đẹp.`
    default:
      return 'Unknown step'
  }
}

export default function TimeLine() {
  // function getSteps() {
  //   return [placename]
  // }

  // function getStepContent(step) {
  //   for (let i = 0; i < todo.length; i++) {
  //     if (step === i) return todo[i]
  //   }
  // }
  const classes = useStyles()
  const [activeStep, setActiveStep] = React.useState(0)
  const steps = getSteps()

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1)
  }

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1)
  }

  const handleReset = () => {
    setActiveStep(0)
  }

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} orientation="vertical" connector={false}>
        {steps.map((label, index) => (
          <Step
            key={label}
            className={classes.step}
            classes={{
              completed: classes.completed,
              active: classes.active,
            }}
          >
            <StepLabel
              classes={{
                completed: classes.completed,
                active: classes.active,
                label: classes.label,
              }}
            >
              {label}
            </StepLabel>
            <StepContent>
              <Typography classes={{ body1: classes.body1 }}>
                {getStepContent(index)}
              </Typography>
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Quay lại
                  </Button>
                  <Button
                    variant="contained"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1
                      ? 'Kết thúc'
                      : 'Đi tiếp nào'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography>
            Chuyến hành trình của bạn đến đây là kết thúc rồi !!!!!
          </Typography>
          <Button onClick={handleReset} className={classes.button}>
            Bắt đầu lại
          </Button>
        </Paper>
      )}
    </div>
  )
}
