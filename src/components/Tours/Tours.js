import React, { useState } from 'react'
import classes from './Tours.module.scss'
import Container from '../Container/Container'
import Card1 from '../../assets/img/trip1.jpg'
import Card2 from '../../assets/img/trip2.jpg'
import Card3 from '../../assets/img/trip3.jpg'
import Card4 from '../../assets/img/trip4.jpg'
import Card5 from '../../assets/img/trip5.jpg'
import Card6 from '../../assets/img/trip6.jpg'
import { Tour } from '../Tour/Tour'
import { Waypoint } from 'react-waypoint'

export const Tours = (props) => {
  const [isEntered, setIsEntered] = useState(false)
  const cards = [Card1, Card2, Card3, Card4, Card5, Card6]
  const handleWaypointEnter = () => {
    setIsEntered(true)
  }
  const handleWaypointLeave = () => {
    setIsEntered(false)
  }
  return (
    <div className={classes.Projects}>
      <Container>
        <Waypoint onEnter={handleWaypointEnter} onLeave={handleWaypointLeave} />
        <div className={classes.TheProjects}>
          <div
            className={`${classes.TheProjectsHeader} ${
              isEntered ? classes.slideInTop : ''
            }`}
          >
            <h1>Điểm đến được ưu thích nhất</h1>
          </div>
          <div className={classes.TheProjectsCards}>
            {cards.map((i) => {
              return <Tour img={i} key={i} text="Đà Nẵng" label="9 tour" />
            })}
          </div>
        </div>
      </Container>
    </div>
  )
}
