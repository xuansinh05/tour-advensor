import React from 'react'
import { Header } from './HomePage/Header/Header'
import Card from 'components/Cards'
import HomePage from './HomePage'
import DetailTour from 'components/DetailTour/DetailTour'

const routes = [
  {
    path: '/',
    exact: true,
    main: () => <HomePage />,
  },
  {
    path: '/card',
    exact: false,
    main: ({ match }) => <Card match={match} />,
  },
  {
    path: '/tour',
    exact: true,
    main: ({ match }) => <Card match={match} />,
  },
  {
    path: '/tour/:id',
    exact: false,
    main: ({ match }) => <DetailTour match={match} />,
  },
]

export default routes
