import React, { useState } from 'react'

import { FiClock, FiStar, FiHeart, FiBriefcase } from 'react-icons/fi'
import classes from './Info.module.scss'
import Container from 'components/Container/Container'
import { Waypoint } from 'react-waypoint'

export const Info = () => {
  const [isEntered, setIsEntered] = useState(false)
  const handleWaypointEnter = () => {
    setIsEntered(true)
  }
  const handleWaypointLeave = () => {
    setIsEntered(false)
  }
  return (
    <>
      <Waypoint onEnter={handleWaypointEnter} onLeave={handleWaypointLeave} />
      <div className={classes.Info}>
        <Container>
          <div className={classes.TheInfo}>
            <div
              className={`${classes.TheInfoContainer} ${
                isEntered ? classes.slideInLeft : ''
              }`}
            >
              <div className={classes.TheInfoContainerImage}>
                <FiBriefcase />
              </div>
              <div className={classes.TheInfoContainerInfo}>
                <p className={classes.Title}>ĐƠN GIẢN</p>
                <p>
                  Chỉ với một vài thao tác, bạn có thể dễ dàng tìm kiếm, đăng
                  một chuyến đi du lịch còn chỗ trống
                </p>
              </div>
            </div>

            <div
              className={`${classes.TheInfoContainer} ${
                isEntered ? classes.slideInRight : ''
              }`}
            >
              <div className={classes.TheInfoContainerImage}>
                <FiStar />
              </div>
              <div className={classes.TheInfoContainerInfo}>
                <p className={classes.Title}>TIẾT KIỆM</p>
                <p>
                  Du lịch chưa bao giờ rẻ đến như vậy. Trải nghiệm du lịch đích
                  thực với mức tiết kiệm không ngờ.
                </p>
              </div>
            </div>
            <div
              className={`${classes.TheInfoContainer} ${
                isEntered ? classes.slideInRight : ''
              }`}
            >
              <div className={classes.TheInfoContainerImage}>
                <FiHeart />
              </div>
              <div className={classes.TheInfoContainerInfo}>
                <p className={classes.Title}>PHONG CÁCH</p>
                <p>
                  Một cách đi du lịch mới thể hiện cá tính và phong cách sống
                  năng động, thân thiện với môi trường.
                </p>
              </div>
            </div>
          </div>
        </Container>
      </div>
    </>
  )
}
