import React, { useState } from 'react'
import { Waypoint } from 'react-waypoint'
import Container from 'components/Container/Container'
import classes from './About.module.scss'
import signImg from '../../../assets/img/apple-icon.png'
export const About = () => {
  const [isEntered, setIsEntered] = useState(false)
  const handleWaypointEnter = () => {
    setIsEntered(true)
  }
  const handleWaypointLeave = () => {
    setIsEntered(false)
  }
  return (
    <section id="about">
      <Waypoint onEnter={handleWaypointEnter} onLeave={handleWaypointLeave} />
      <Container>
        <div className={`${classes.About} `}>
          <div
            className={`${classes.AboutHeader} ${
              isEntered ? classes.slideInTop : ''
            }`}
          >
            <h1>Về chúng tôi</h1>
          </div>
          <div
            className={`${classes.AboutParagraph} ${
              isEntered ? classes.slideInTop : ''
            }`}
          >
            <p>
              Chúng tôi sẽ tư vấn lịch trình củng như các tour tốt nhất cho bạn.
              Bạn sẽ tận hưởng kì nghỉ dưỡng và du lịch cùng bạn bè người thân
              một cách trọn tốt nhất
            </p>
          </div>
          <div
            className={`${classes.AboutSign} ${
              isEntered ? classes.slideInBottom : ''
            }`}
          >
            <img src={signImg} alt="sign" />
          </div>
        </div>
      </Container>
    </section>
  )
}
