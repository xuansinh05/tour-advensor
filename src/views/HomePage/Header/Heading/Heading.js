import React, { useState } from 'react'
import classes from './Heading.module.scss'
import { MdKeyboardArrowDown } from 'react-icons/md'
import { HashLink as Link } from 'react-router-hash-link'
import { Waypoint } from 'react-waypoint'

export const Heading = () => {
  const [isEntered, setIsEntered] = useState(false)
  const handleWaypointEnter = () => {
    setIsEntered(true)
  }
  const handleWaypointLeave = () => {
    setIsEntered(false)
  }
  return (
    <div className={`${classes.Heading} `}>
      <Waypoint
        bottomOffset="15px"
        onEnter={handleWaypointEnter}
        onLeave={handleWaypointLeave}
      />
      <div
        className={`${classes.HeadingContent} ${
          isEntered ? classes.slideInTop : ''
        }`}
      >
        <h1>Cuộc sống là những trải nghiệm và hành trình</h1>
        <h2>
          Đến với chúng tôi sẽ giúp bạn có lịch trình du lịch tốt nhất <br />
          <span>Tour and trip with your team</span>
        </h2>

        <button className={classes.HeadingContentButton}>Contact</button>
      </div>
    </div>
  )
}
