import React, { useState } from 'react'
import { Waypoint } from 'react-waypoint'
import { NavLink } from 'react-router-dom'
import logo from '../../../../assets/img/logo.png'
import { HashLink as Link } from 'react-router-hash-link'
import classes from './Nav.module.scss'
import Container from '../../../../components/Container/Container'

export const Nav = (props) => {
  const [isEntered, setIsEntered] = useState(false)
  const handleWaypointEnter = () => {
    setIsEntered(true)
  }
  const handleWaypointLeave = () => {
    setIsEntered(false)
  }
  return (
    <>
      <Waypoint onEnter={handleWaypointEnter} onLeave={handleWaypointLeave} />
      <div className={`${classes.Nav} ${classes.bgWhite}`}>
        <Container>
          <div className={classes.flex}>
            <div
              className={`${classes.NavLogo} ${
                isEntered ? classes.slideInLeft : ''
              }`}
            >
              <NavLink to="/" className={classes.NavItemsItem}>
                <img className={classes.NavLogoImg} src={logo} alt="logo" />
              </NavLink>
            </div>
            <div
              className={`${classes.NavItems} ${
                isEntered ? classes.slideInRight : ''
              }`}
            >
              <NavLink
                to="/"
                activeStyle={{ color: 'red' }}
                className={`${classes.NavItemsItem} ${classes.ItemColorBlack}`}
              >
                Home
              </NavLink>
              <Link
                to="/#about"
                className={`${classes.NavItemsItem} ${classes.ItemColorBlack}`}
              >
                About
              </Link>
              <NavLink
                to="/tour"
                activeStyle={{ color: 'red' }}
                className={`${classes.NavItemsItem} ${classes.ItemColorBlack}`}
              >
                Tour
              </NavLink>
              <NavLink
                activeStyle={{ color: 'red' }}
                to="/blog"
                className={`${classes.NavItemsItem} ${classes.ItemColorBlack}`}
              >
                blog
              </NavLink>
              <NavLink
                activeStyle={{ color: 'red' }}
                to="/login"
                className={`${classes.NavItemsItem} ${classes.ItemColorBlack}`}
              >
                Sign in
              </NavLink>
              <NavLink
                activeStyle={{ color: 'red' }}
                to="/register"
                className={`${classes.NavItemsItem} ${classes.ItemColorBlack}`}
              >
                Sign up
              </NavLink>
            </div>
          </div>
        </Container>
      </div>
    </>
  )
}
