import React from 'react'
import classes from './Footer.module.scss'

import { FaTwitter, FaFacebookF, FaInstagram, FaGithub } from 'react-icons/fa'
import Container from '../../../components/Container/Container'

export const Footer = () => {
  return (
    <footer className={classes.Background}>
      <Container>
        <div className={classes.Footer}>
          <div className={classes.Footer_Icons}>
            <a
              href="https://twitter.com/SinhNgu22651238"
              target="_blank"
              className={classes.Footer_Icons_Icon}
              rel="noopener noreferrer"
            >
              <FaTwitter />
            </a>
            <a
              href="https://www.facebook.com/xuansinh123/"
              target="_blank"
              className={classes.Footer_Icons_Icon}
              rel="noopener noreferrer"
            >
              <FaFacebookF />
            </a>
            <a
              href="https://www.instagram.com/xuan.sinh/"
              target="_blank"
              rel="noopener noreferrer"
              className={classes.Footer_Icons_Icon}
            >
              <FaInstagram />
            </a>
            <a
              href="https://github.com/xuansinh05"
              target="_blank"
              rel="noopener noreferrer"
              className={classes.Footer_Icons_Icon}
            >
              <FaGithub />
            </a>
          </div>
          <p>Design by Sinh Dat</p>
        </div>
      </Container>
    </footer>
  )
}
