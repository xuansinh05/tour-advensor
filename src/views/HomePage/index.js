import React from 'react'
import { About } from '../HomePage/About/About'
import { Info } from './Info/Info'
import { Tours } from '../../components/Tours/Tours'
import Card from 'components/Cards'
import { Header } from './Header/Header'

export default function HomePage() {
  return (
    <div>
      <Header />
      <About />
      <Info />
      <Tours />
      {/* <Card /> */}
    </div>
  )
}
