import { getMapServiceByName } from './service-map'
import MapService from './map-service'

const useMapServices = ({ names = [], services: userServices } = {}) => {
  const services = names.map((name) => getMapServiceByName(name, userServices))

  return services.map((service) => new MapService(service))
}

export default useMapServices
