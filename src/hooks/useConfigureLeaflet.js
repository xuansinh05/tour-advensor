import L from 'leaflet'
import mark1 from 'leaflet/dist/images/marker-icon-2x.png'
import mark2 from 'leaflet/dist/images/marker-icon.png'
import mark3 from 'leaflet/dist/images/marker-shadow.png'

import { isDomAvailable } from './util'

const useConfigureLeaflet = () => {
  if (!isDomAvailable()) return

  delete L.Icon.Default.prototype._getIconUrl

  L.Icon.Default.mergeOptions({
    iconRetinaUrl: mark1,
    iconUrl: mark2,
    shadowUrl: mark3,
  })
}

export default useConfigureLeaflet
